"use strict";

let themeBtn = document.querySelector(".themeChangeBtn");

themeBtn.addEventListener("click", () => {
  //WRITING LOCALSTORAGE
  if (document.body.classList.contains("light")) {
    localStorage.setItem("theme", "dark");
  } else {
    localStorage.setItem("theme", "light");
  }

  // ACTUAL THEME CHANGE
  if (localStorage.getItem("theme") === "dark") {
    document.body.classList.remove("light");
    document.body.classList.add("dark");
    themeBtn.innerText = "light";
  } else {
    document.body.classList.remove("dark");
    document.body.classList.add("light");
    themeBtn.innerText = "dark";
  }
});

// USING LOCALSTORAGE IN CLOSED/OPENED TABS
document.addEventListener("DOMContentLoaded", () => {
  if (localStorage.getItem("theme") === "dark") {
    document.body.classList.remove("light");
    document.body.classList.add("dark");
    themeBtn.innerText = "light";
  } else {
    document.body.classList.remove("dark");
    document.body.classList.add("light");
    themeBtn.innerText = "dark";
  }
});
